//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PizzaHeavenAPI.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Topping
    {
        public string ToppingName { get; set; }
        public string ToppingDescription { get; set; }
        public Nullable<decimal> ToppingPrice { get; set; }
        public Nullable<decimal> HeatRating { get; set; }
        public string ToppingImage { get; set; }
    }
}
